<?php

use App\Models\Item;
use App\Models\Restaurant;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReplicateItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $itemsDelete = Item::where('restaurant_id', '!=', 5)->delete();

        // Replicate
        foreach ($this->getRestaurants() as $restaurant)
        {
            $getItems = Item::where('restaurant_id', 5)->get();
            foreach ($getItems as $item) {
                $replicateItem = $item->replicate();
                $replicateItem->restaurant_id = $restaurant->id;
                $replicateItem->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

    private function getRestaurants()
    {
        $restaurants = Restaurant::where('id', '!=', 5)->get();
        return $restaurants;
    }
}
